//import library to help create component
import React from 'react';
import { View, StatusBar } from 'react-native';
//custom imports
import Header from './src/components/Header';
import AlbumList from './src/components/AlbumList';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.StatusBar}>
          <StatusBar />
        </View>
        <Header headerTitle='Albums' />
        <AlbumList />
      </View>
    );
  }
}

const styles = {
  container: {
    paddingBottom: 80
  },
  StatusBar: {
    height: StatusBar.currentHeight
  }
};
