import React from 'react';
import { ScrollView } from 'react-native';
import AlbumDetails from './AlbumDetails';

//create component
export default class AlbumList extends React.Component {
	state = {
		albums: []
	};

	componentWillMount() {
		fetch('http://rallycoding.herokuapp.com/api/music_albums')
			.then(response => response.json()).then(responseJson => {
				this.setState({
					albums: responseJson
				})
			})
	}

	rendeDetailsView() {
		return this.state.albums.map((album) => {
			return <AlbumDetails key={album.title} album={album} />
		});
	}

	render() {
		return (
				<ScrollView>
						{ this.rendeDetailsView() }
				</ScrollView>
		);
	}
}
