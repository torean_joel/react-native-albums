import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

//create component
const AlbumDetails = (props) => {
  const album = props.album;

	return (
			<Card>
        <CardSection>
          <View style={styles.thumbnailContainerStyle}>
            <Image 
              style={styles.thumbnailStyle} 
              source={{uri: album.thumbnail_image}} />
          </View>
          <View style={styles.headerContentStyle}>
            <Text style={styles.headerTextStyle}>{album.title}</Text>
            <Text>{album.artist}</Text>
          </View>
        </CardSection>

        <CardSection>
          <Image 
            style={styles.albumCoverStyle} 
            source={{uri: album.image}} />
        </CardSection>

        <CardSection>
          <Button whenPressed={() => Linking.openURL(album.url)}>Buy on Amazon</Button>
        </CardSection>
			</Card>
	);
}

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
  },
  albumCoverStyle: {
    height: 300,
    flex: 1,
    width: null, //this iwll make it full width
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  }
}

export default AlbumDetails;
