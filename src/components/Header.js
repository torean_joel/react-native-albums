import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

//create component
export default class Header extends React.Component {
  render() {
    return (
        <View style={styles.headerContainer}>
            <Text style={styles.headerTitle}>{ this.props.headerTitle }</Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: '#f8f8f8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        elevation: 2,
        position: 'relative'
    },
    headerTitle: {
        fontSize: 20,
    }
});
